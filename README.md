# What's in here?

Draw.io was used to create the schema and the schema can be opened on draw.io

BIO.sql is the database script (includes the "CREATE DATABASE" statement)


# Database Schema
![](Esquema_projeto_3rd.png)


# Description of the type values

Name of the type value  | Description
------------ | -------------
bigint | Large-range integer, it contains the "nextval" function to automaticly generate the next value without the user input 
text | Variable unlimited length
char | Fixed-length (1 character)


# Description of the DATABASE tables

 
## Dataset 

This table will store the dataset information.

Name of the attribute   | Description
------------ | -------------
id_dataset | Unique identifier of the dataset
paper_name | Name of the paper
author_name | Name of the authors of the paper


## Seq_Not_Aligned

This table will store the sequence not aligned information. 

Name of the attribute   | Description
------------ | -------------
id_seq_not_aligned | Unique identifier of the not aligned sequence
org_sequence | Organism sequence
id_dataset | Unique identifier of the dataset from where the sequence came
accession_number | Unique identifier given to a biological polymer sequence


## Seq_Aligned

This table will store the aligned sequences information.

Name of the attribute   | Description
------------ | -------------
id_seq_aligned | Unique identifier of the aligned sequence
org_sequence | Organism sequence
id_seq_not_aligned | Unique identifier of the dataset from where the sequence came


## Seq_(10_NR, 10_R, 15_NR, 15_R, 20_NR, 20_R)

This table(s) will store the sequences, with randomized and non-randomized missing data, information.

Name of the attribute   | Description
------------ | -------------
id_seq_(10_NR, 10_R, 15_NR, 15_R, 20_NR, 20_R) | Unique identifier of the sequence
org_sequence | Organism sequence
id_seq_aligned | Unique identifier of the aligned sequence


## Tree_Aligned

This table(s) will store the trees, of the sequences without missing data, information.

Name of the attribute   | Description
------------ | -------------
id_tree_tree_aligned | Unique identifier of the aligned tree
phyl_tree | Phylogenetic tree (svg format)
id_seq_aligned | Unique identifier of the aligned sequence


## Tree_(10_NR, 10_R, 15_NR, 15_R, 20_NR, 20_R)

This table(s) will store the trees, of the sequences with missing data, information.

Name of the attribute   | Description
------------ | -------------
id_tree_tree_aligned | Unique identifier of the aligned tree
phyl_tree | Phylogenetic tree (svg format)
id_seq_(10_NR, 10_R, 15_NR, 15_R, 20_NR, 20_R) | Unique identifier of the sequence


