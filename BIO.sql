-- Database: bio_proj

-- DROP DATABASE bio_proj;

CREATE DATABASE bio_proj
    WITH 
    OWNER = postgres
    ENCODING = 'UTF8'
    LC_COLLATE = 'en_US.utf8'
    LC_CTYPE = 'en_US.utf8'
    TABLESPACE = pg_default
    CONNECTION LIMIT = -1;


-- SCHEMA: bio

-- DROP SCHEMA bio ;

CREATE SCHEMA bio
    AUTHORIZATION postgres;


-- Table: bio.Datasets

-- DROP TABLE bio."Datasets";

CREATE TABLE bio."Datasets"
(
    "Dataset_id" bigint NOT NULL DEFAULT nextval('bio."Datasets_Dataset_id_seq"'::regclass),
    "Names" text COLLATE pg_catalog."default",
    "Authors" text COLLATE pg_catalog."default",
    "Acession_number" text COLLATE pg_catalog."default",
    "Link" text COLLATE pg_catalog."default",
    "DNA_seq" text COLLATE pg_catalog."default",
    "Seq_name" text COLLATE pg_catalog."default",
    "DNA_seq_random_10" text COLLATE pg_catalog."default",
    "DNA_seq_random_15" text COLLATE pg_catalog."default",
    "DNA_seq_random_20" text COLLATE pg_catalog."default",
    "Num_id" bigint NOT NULL DEFAULT nextval('bio."Datasets_Num_id_seq"'::regclass),
    CONSTRAINT "Datasets_pkey" PRIMARY KEY ("Dataset_id")
)

TABLESPACE pg_default;

ALTER TABLE bio."Datasets"
    OWNER to postgres;



-- Table: bio.Iqtree

-- DROP TABLE bio."Iqtree";

CREATE TABLE bio."Iqtree"
(
    "Dataset_id" bigint NOT NULL DEFAULT nextval('bio."Iqtree_Dataset_id_seq"'::regclass),
    iqtree_random_10 text COLLATE pg_catalog."default",
    iq_tree_random_15 text COLLATE pg_catalog."default",
    iq_tree_random_20 text COLLATE pg_catalog."default",
    CONSTRAINT "Iqtree_pkey" PRIMARY KEY ("Dataset_id")
)

TABLESPACE pg_default;

ALTER TABLE bio."Iqtree"
    OWNER to postgres;



-- Table: bio.Trees

-- DROP TABLE bio."Trees";

CREATE TABLE bio."Trees"
(
    "Dataset_id" bigint NOT NULL DEFAULT nextval('bio."Trees_Dataset_id_seq"'::regclass),
    "SVG_ml_default" text COLLATE pg_catalog."default",
    "SVG_bayes_default" text COLLATE pg_catalog."default",
    "SVG_ml_10" text COLLATE pg_catalog."default",
    "SVG_bayes_10" text COLLATE pg_catalog."default",
    "SVG_ml_15" text COLLATE pg_catalog."default",
    "SVG_bayes_15" text COLLATE pg_catalog."default",
    "SVG_ml_20" text COLLATE pg_catalog."default",
    "SVG_bayes_20" text COLLATE pg_catalog."default",
    CONSTRAINT "Trees_pkey" PRIMARY KEY ("Dataset_id")
)